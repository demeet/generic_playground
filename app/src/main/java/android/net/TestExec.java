package android.net;

import android.content.Context;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;

public class TestExec {

    private static final String ETHERNET_MANAGER_NAME = "android.net.EthernetManager";
    private static final String ETHERNET_MANAGER_SERVICE = "ethernet";
    private static final String GET_CONFIGURATION_METHOD = "getConfiguration";


    Context context;

    public TestExec(Context context) {
        this.context = context;
    }

    public String getCurrentIp() {
//        IpConfiguration ipConfiguration = ethernetManager.
//        if (ethernetManager instanceof EthernetManager){
//            ethernetManager.
//        }


        try {

            Runtime.getRuntime().exec("su");
            Class cls = Class.forName(ETHERNET_MANAGER_NAME);

            //this will get the public constructors for the class
            Constructor[] publicConstructors = cls.getConstructors();

            //this will get all the declared constructors for the class
            Constructor[] declaredConstructors = cls.getDeclaredConstructors();

            //this will return only the public methods
            Method[] methods = cls.getMethods();
            //this will return all the methods declared in the class
            Method[] declaredMethods = cls.getDeclaredMethods();

            //this will get all the public fields for the class

            Field[] publicFields = cls.getFields();
            //this will get all the declared fields for the class
            Field[] declaredFields = cls.getDeclaredFields();

            Object clsObject = context.getSystemService(ETHERNET_MANAGER_SERVICE);

            //to get a specific method at the runtime pass the name of the method
            Method method = cls.getDeclaredMethod(GET_CONFIGURATION_METHOD);

            //since the method in question is static the object on which it needs to be called is null
            //else you can pass `clsObject` from above as the parameter to the method below
//            getStrIp(method.invoke(clsObject));
//            getIpAddress(method.invoke(clsObject));

            Object before = method.invoke(clsObject);

            setDhcpMode();

            Object after = method.invoke(clsObject);

            Object dfg = null;


            //take action as the module you were looking for exists
        } catch (ClassNotFoundException e) {
            //add your fallback mechanism here
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            //add your fallback mechanism here
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void setDhcpMode() throws IOException {
        enterCommand("ifconfig eth0 default");
    }

    private void setStaticMode() throws IOException {
        enterCommand("ifconfig eth0 192.198.1.25/8");
    }

    private void enterCommand(String command) throws IOException {
        Runtime.getRuntime().exec(command);
    }

    private Object setNewIp(Object ipConfigObj) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException, InstantiationException {

        Object staticIpConfig = ipConfigObj.getClass().getDeclaredMethod("getStaticIpConfiguration").invoke(ipConfigObj);

        Field ipAddress = staticIpConfig.getClass().getDeclaredField("ipAddress");

        Constructor constructor = LinkAddress.class.getConstructor(String.class);

        constructor.setAccessible(true);

        Object address = constructor.newInstance("192.168.12.15/5");
        ipAddress.set(staticIpConfig, address);


        ipConfigObj.getClass().getDeclaredMethod("setStaticIpConfiguration", staticIpConfig.getClass())
                .invoke(ipConfigObj, staticIpConfig);

        return ipConfigObj;

    }


    private String getStrIp(Object ipConfigObj) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        Class ipConfClass = Class.forName(ipConfigObj.getClass().getName());

        Method toStr = ipConfClass.getDeclaredMethod("toString");

        String strIpConf = (String) toStr.invoke(ipConfigObj);

        return "asd";
    }

    private InetAddress getIpAddress(Object ipConfigurationObj) throws ClassNotFoundException, NoSuchMethodException, NoSuchFieldException, InvocationTargetException, IllegalAccessException, InstantiationException {

        Class ipConfCls = Class.forName(ipConfigurationObj.getClass().getName());


        Field ipAssignment = ipConfCls.getField("ipAssignment");

        Object o = ipAssignment.getType().getEnumConstants();

        Object fieldValue = ipAssignment.get(ipConfigurationObj);

        Method valueOf = ipAssignment.getType().getMethod("valueOf", String.class);

        Object o1 = valueOf.invoke(null, "STATIC");


        Method[] ipConfMethods = ipConfCls.getDeclaredMethods();

        Method getIpMethod = ipConfCls.getMethod("getStaticIpConfiguration");
        return null;
    }
}
