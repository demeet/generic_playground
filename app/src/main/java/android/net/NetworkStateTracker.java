/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.net;

import android.content.Context;
import android.os.Handler;


/**
 * Each subclass of this class keeps track of the state of connectivity
 * of a network interface. All state information for a network should
 * be kept in a Tracker class. This superclass manages the
 * network-type-independent aspects of network state.
 *
 * {@hide}
 */
public abstract class NetworkStateTracker extends Handler {

    public static final int EVENT_STATE_CHANGED = 1;
    public static final int EVENT_SCAN_RESULTS_AVAILABLE = 2;
    /**
     * arg1: 1 to show, 0 to hide
     * arg2: ID of the notification
     * obj: Notification (if showing)
     */
    public static final int EVENT_NOTIFICATION_CHANGED = 3;
    public static final int EVENT_CONFIGURATION_CHANGED = 4;
    public static final int EVENT_ROAMING_CHANGED = 5;
    public static final int EVENT_NETWORK_SUBTYPE_CHANGED = 6;

    public NetworkStateTracker(Context context,
            Handler target,
            int networkType,
            int subType,
            String typeName,
            String subtypeName) {
    	throw new RuntimeException("Stub!");
    }

    public NetworkInfo getNetworkInfo() {
    	throw new RuntimeException("Stub!");
    }

    /**
     * Return the system properties name associated with the tcp buffer sizes
     * for this network.
     */
    public abstract String getTcpBufferSizesPropName();

    /**
     * Return the IP addresses of the DNS servers available for the mobile data
     * network interface.
     * @return a list of DNS addresses, with no holes.
     */
    public String[] getNameServers() {
    	throw new RuntimeException("Stub!");
    }

    public void addPrivateDnsRoutes() {
    	throw new RuntimeException("Stub!");
    }

    public void removePrivateDnsRoutes() {
    	throw new RuntimeException("Stub!");
    }

    public void addDefaultRoute() {
    	throw new RuntimeException("Stub!");
    }

    public void removeDefaultRoute() {
    	throw new RuntimeException("Stub!");
    }

    /**
     * Reads the network specific TCP buffer sizes from SystemProperties
     * net.tcp.buffersize.[default|wifi|umts|edge|gprs] and set them for system
     * wide use
     */
   public void updateNetworkSettings() {
   	throw new RuntimeException("Stub!");
    }

    /**
     * Release the wakelock, if any, that may be held while handling a
     * disconnect operation.
     */
    public void releaseWakeLock() {
    	throw new RuntimeException("Stub!");
    }

    /**
     * Record the detailed state of a network, and if it is a
     * change from the previous state, send a notification to
     * any listeners.
     * @param state the new @{code DetailedState}
     */
    public void setDetailedState(NetworkInfo.DetailedState state) {
    	throw new RuntimeException("Stub!");
    }

    /**
     * Record the detailed state of a network, and if it is a
     * change from the previous state, send a notification to
     * any listeners.
     * @param state the new @{code DetailedState}
     * @param reason a {@code String} indicating a reason for the state change,
     * if one was supplied. May be {@code null}.
     * @param extraInfo optional {@code String} providing extra information about the state change
     */
    public void setDetailedState(NetworkInfo.DetailedState state, String reason, String extraInfo) {
    	throw new RuntimeException("Stub!");
    }

    public void setTeardownRequested(boolean isRequested) {
    	throw new RuntimeException("Stub!");
    }

    public boolean isTeardownRequested() {
    	throw new RuntimeException("Stub!");
    }

    public abstract void startMonitoring();

    /**
     * Disable connectivity to a network
     * @return {@code true} if a teardown occurred, {@code false} if the
     * teardown did not occur.
     */
    public abstract boolean teardown();

    /**
     * Reenable connectivity to a network after a {@link #teardown()}.
     */
    public abstract boolean reconnect();

    /**
     * Turn the wireless radio off for a network.
     * @param turnOn {@code true} to turn the radio on, {@code false}
     */
    public abstract boolean setRadio(boolean turnOn);

    /**
     * Returns an indication of whether this network is available for
     * connections. A value of {@code false} means that some quasi-permanent
     * condition prevents connectivity to this network.
     */
    public abstract boolean isAvailable();

    /**
     * Tells the underlying networking system that the caller wants to
     * begin using the named feature. The interpretation of {@code feature}
     * is completely up to each networking implementation.
     * @param feature the name of the feature to be used
     * @param callingPid the process ID of the process that is issuing this request
     * @param callingUid the user ID of the process that is issuing this request
     * @return an integer value representing the outcome of the request.
     * The interpretation of this value is specific to each networking
     * implementation+feature combination, except that the value {@code -1}
     * always indicates failure.
     */
    public abstract int startUsingNetworkFeature(String feature, int callingPid, int callingUid);

    /**
     * Tells the underlying networking system that the caller is finished
     * using the named feature. The interpretation of {@code feature}
     * is completely up to each networking implementation.
     * @param feature the name of the feature that is no longer needed.
     * @param callingPid the process ID of the process that is issuing this request
     * @param callingUid the user ID of the process that is issuing this request
     * @return an integer value representing the outcome of the request.
     * The interpretation of this value is specific to each networking
     * implementation+feature combination, except that the value {@code -1}
     * always indicates failure.
     */
    public abstract int stopUsingNetworkFeature(String feature, int callingPid, int callingUid);

    /**
     * Ensure that a network route exists to deliver traffic to the specified
     * host via this network interface.
     * @param hostAddress the IP address of the host to which the route is desired
     * @return {@code true} on success, {@code false} on failure
     */
    public boolean requestRouteToHost(int hostAddress) {
    	throw new RuntimeException("Stub!");
    }

    /**
     * Interprets scan results. This will be called at a safe time for
     * processing, and from a safe thread.
     */
    public void interpretScanResultsAvailable() {
    	throw new RuntimeException("Stub!");
    }

    public String getInterfaceName() {
    	throw new RuntimeException("Stub!");
    }
}
