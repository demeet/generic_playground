package com.example.iptest.generics;

public class GenericUtils {


    public static <T> boolean isMoxySupported(T t) {
        return t instanceof Integer && (int) t < 2019;
    }
}
