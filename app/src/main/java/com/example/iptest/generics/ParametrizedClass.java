package com.example.iptest.generics;

import android.util.Log;
import com.example.iptest.Box;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BoundedClass<T extends Number & Comparable<T> & Appendable> {/*...*/}


    private T t;

    public boolean isEven() {
        return t.intValue() % 2 == 0; // you can invoke methods defined in the bounds
    }


    public void setSomething(T t) {/*...*/}

    public T returnSomething(int value) {
        return t;
    }


    public static <T> T getFirst(List<? super T> list) {
        return list.get(0); // compile-time error
    }


    private void invoke() {
        Number number;
        number = 10; // OK
        number = 1.1; // OK

        Box<Number> box = new Box<>();

        box.add(10); // OK
        box.add(1.1); // OK

//        compile-time error
        box.addAnotherList(new ArrayList<Integer>());
        box.addAnotherList(new ArrayList<Double>());


        Box<? extends Number> numbers = new Box<>();
        Box<Integer> integers = new Box<>();
        integers.add(1);
        integers.add(2);
        numbers = integers;


        List<Integer> ints = new ArrayList<Integer>();
        ints.add(1);
        ints.add(2);
        List<? extends Number> nums = ints;
        nums.add(3.14); // compile-time error


        List<String> strs = new ArrayList<String>();
        List<Object> objs = strs; // !!! The cause of the upcoming problem sits here. Java prohibits this!
        objs.add(1); // Here we put an Integer into a list of Strings
        String s = strs.get(0); // !!! ClassCastException: Cannot cast Integer to String


    }

    public static void reverse(List<?> list) {
        List<Object> tmp = new ArrayList<Object>(list);
        for (int i = 0; i < list.size(); i++) {
            list.set(i, tmp.get(list.size() - i - 1)); // compile-time error
        }
    }

    private <U, K> void hateMoxy(U u, K k) {
        Log.d(TAG, u.toString() + " " + k.toString());
    }

    private static final String TAG = BoundedClass.class.getSimpleName();

    public int compareTo(T o) {
        return 0;
    }


    public <T> List<T> genericMethod(List<T> list) {
        return list.stream().collect(Collectors.toList());
    }

    //    erase to

    public <T> List<T> genericMethod(List list) {
        return list.stream().collect(Collectors.toList());
    }


}
