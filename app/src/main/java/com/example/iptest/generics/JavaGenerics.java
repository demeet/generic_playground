package com.example.iptest.generics;

import java.util.ArrayList;
import java.util.List;

public class JavaGenerics {

    Object[] elementData;

    private void test() {

        List<String> list = new ArrayList<>();
        list.add(""); //OK

        //it doesn't compile
        list.add(0);
        list.add(1.1);

        String s = list.get(0); //OK





        Box<String> box = new Box<>("");
        Box<Integer> box1 = new Box<Integer>(1);





    }

    public Object get(int index) {
        return elementData[index];
    }

}
