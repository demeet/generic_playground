package com.example.iptest.generics

fun <T> singletonList(item: T): List<T> {/*...*/
}

val test1 = singletonList<Int>(10)
//or
val test2 = singletonList(10)

//but
val emptyList = mutableListOf<String>()

class Foo<T : Number>(t: T) {
    var foo = t
}




val <T> List<T>.prop: T
    get() = this[size - 2]





class Bar<T>(t: T)
        where T : Comparable<T>,
              T : Number,
              T : Appendable {
    var foo = t
}






fun <T> isA(list: List<T>) = list is List<String> // Cannot check for instance of erased type: List<String>

fun <T> isA(value: Any) = value is T // Cannot check for instance of erased type: T

fun <reified T> iSA(value: Any) =
    value is T // Compile time error: Only type parameters of inline functions can be reified

fun invoke() {
    val nullable: String? = "abc"
    var nonNull: String = nullable //Type mismatch. Required:String, Found:String?
}


//    covariance

// invariant class Box in type parameter T
class Box<T>(private var value: T) {

    fun get() = value
    fun set(value: T) {
        this.value = value
    }
}


// contravariance

class Checker<in T : Number> {
    fun isNull(value: T) = value != 0
}


fun <T> copyData(
    source: Box<out T>,
    destination: Box<in T>
) {
    destination.set(source.get())
}

fun testTypeProjection() {

    val source: Box<Int> = Box(1)
    val destination: Box<Any> = Box(0)

    copyData<Number>(source, destination)
}

// star projection


fun doSomethingWithList(list: MutableList<*>) {
    list.clear()
    list.first()
    list.last()
    list.get(0)

    list.add(25) // The integer literal does not conform to the expected type Nothing
}

fun testStarProjection() {
    val list = mutableListOf(1, 2, 3)

    doSomethingWithList(list)
}

















