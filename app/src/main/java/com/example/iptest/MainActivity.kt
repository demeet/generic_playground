package com.example.iptest

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method
import java.net.InetAddress
import java.net.NetworkInterface
import java.net.SocketException
import java.util.*

inline fun <reified T : Activity> Context.startActivity() {
    val intent = Intent(this, T::class.java)
    startActivity(intent)
}

class MainActivity : AppCompatActivity() {


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startBtn.setOnClickListener {
            //            val s = getIpAddress()
//            Toast.makeText(this, "eth enable : ${isEthOn()}", Toast.LENGTH_SHORT).show()
//            val instan= getSystemService(Context.CONNECTIVITY_SERVICE)
//             TestExec(this).currentIp
//            var intent = Intent(this,)
            startActivity(Intent(Settings.ACTION_SETTINGS))
        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getNetConfig() = run {
        val p = Runtime.getRuntime().exec("su")

        var emClass: Class<*>? = null
        try {
            emClass = Class.forName("android.net.ethernet.EthernetManager")
        } catch (e: ClassNotFoundException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }

        var meth: Method? = null
        val emInstance = getSystemService("ethernet")

        val methodSetEthEnabled: Array<Method>
        methodSetEthEnabled = emClass!!.getDeclaredMethods()
        for (m in methodSetEthEnabled) {
            if (m.name.contains("getConfiguration")) {
                meth = m
            }
        }

        meth?.isAccessible = true
//        methodSetEthEnabled!!.setAccessible(true)
        try {
            // new Boolean(true) to enable, new Boolean(false) to disable
            var ob = meth?.invoke(emInstance, "eth0")


            Log.d("asd", "asdsd")
        } catch (e: IllegalArgumentException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        } catch (e: InvocationTargetException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }
    }

    private fun getIpAddress(): String {
        val networkInterface = NetworkInterface.getByName("eth0")
        val enumeration: Enumeration<InetAddress> = networkInterface.inetAddresses
        return enumeration.nextElement().hostAddress

    }

    fun doesEthExist(): Boolean {
        val list = getListOfNetworkInterfaces()

        return list!!.contains("eth0")

    }

    fun getListOfNetworkInterfaces(): List<String>? {

        val list = ArrayList<String>()

        val nets: Enumeration<NetworkInterface>

        try {
            nets = NetworkInterface.getNetworkInterfaces()
        } catch (e: SocketException) {

            e.printStackTrace()
            return null
        }

        for (netint in Collections.list(nets)) {

            list.add(netint.getName())
        }

        return list
    }

    fun isEthOn(): Boolean {
        try {
            var line: String
            var r = false

            val p = Runtime.getRuntime().exec(arrayOf("su", "netcfg"))
            val input = BufferedReader(InputStreamReader(p.inputStream))

            val inp = p.inputStream
            while (input.readLine().also { line = it } == null) {
                if (line.contains("eth0")) {
                    r = line.contains("UP")
                    break
                }
            }
            input.close()

            Log.e("OLE", "isEthOn: $r")
            return r

        } catch (e: IOException) {
            Log.e("OLE", "Runtime Error: " + e.toString())
            e.printStackTrace()
            return false
        }



    }

    override fun onStart() {
        super.onStart()
        startActivity(Intent(this, MainActivity::class.java))

        startActivity<MainActivity>()
    }
//
//    fun turnEthOnOrOff() {
//        try {
//
//            if (isEthOn()) {
//                Runtime.getRuntime().exec("ifconfig eth0 down")
//
//            } else {
//                Runtime.getRuntime().exec("ifconfig eth0 up")
//            }
//
//        } catch (e: IOException) {
//            InternalAccessor.
//        }
//            Log.e("OLE", "Runtime Error: " + e.message)
//            e.printStackTrace()
//        }
//    }
//
//    private fun connectToStaticSettingsViaIfconfig(scs: StaticConnectionSettings): Boolean {
//        try {
//            if (typeChosen.equalsIgnoreCase("dhcp")) {
//                Runtime.getRuntime().exec("ifconfig eth0 dhcp start")
//            } else {
//
//                Runtime.getRuntime()
//                    .exec("ifconfig eth0 " + scs.getIp() + " netmask " + scs.getNetmask() + " gw " + scs.getGateway())
//            }
//        } catch (e: IOException) {
//            Log.e("OLE", "Runtime Error: " + e.message)
//            e.printStackTrace()
//            return false
//        }
//        return true
//    }

    inner class StaticConnectionSettings() {

        private val ip: String? = null
        private val netmask: String? = null
        private val dns: String? = null
        private val mac: String? = null
        private val gateway: String? = null
        private val type: String? = null

    }
}