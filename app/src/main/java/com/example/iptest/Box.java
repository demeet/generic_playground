package com.example.iptest;

import java.util.ArrayList;
import java.util.List;

public class Box<T> extends ArrayList<T> {

    @Override
    public boolean add(T t) {
        return super.add(t);
    }

    public void addAnotherList(List<T> list){
        addAll(list);
    }
}
